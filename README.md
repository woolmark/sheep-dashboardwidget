Sheep for Dashboard Widget
----------------------------------------------------------------------------
Sheep for Dashboard Widget is an implementation [sheep] for Mac OS X Dashboard Widget.

License
----------------------------------------------------------------------------
[wtfpl] 

[sheep]: https://bitbucket.org/runne/woolmark "Sheep"
[wtfpl]: http://www.wtfpl.net "WTFPL"
