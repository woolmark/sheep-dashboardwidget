/**
 * @license WTFPL
 * @file
 */

/**
 * sheep.
 *
 * @constructor
 */
var Sheep = function() {
    this.dcanvas = document.getElementById("woolmark_canvas");
    this.canvas = document.createElement("canvas");
    this.canvas.width = 100;
    this.canvas.height = 100;

    this.runnable = true;

    this.sheep_image = [ new Image(), new Image() ];
    this.background = new Image();

    this.sheep_pos = [];
    for(var i = 0; i < 100; i++) {
        this.sheep_pos[i] = [];
        this.sheep_pos[i][0] = 0;
        this.sheep_pos[i][1] = -1;
        this.sheep_pos[i][2] = 0;
        this.sheep_pos[i][3] = 0;
    }

    this.sheep_pos[0][0] = this.canvas.width + 10;
    this.sheep_pos[0][1] = this.canvas.height - 40;
    this.sheep_pos[0][3] = this.getJumpX(this.sheep_pos[0][1]);

    this.add_sheep;

    this.action = 0;

    this.sheep_number = 0;
};

/**
 * start animation.
 */
Sheep.prototype.start = function() {
    var self = this;

    this.runnable = true;

    this.background.src = "background.png";
    this.sheep_image[0].src = "sheep00.png";
    this.sheep_image[1].src = "sheep01.png";

    this.dcanvas.addEventListener("mousedown",  function() {
        self.onMouseDown();
    }, false);
    this.dcanvas.addEventListener("mouseup",  function() {
        self.onMouseUp();
    }, false);
    this.dcanvas.addEventListener("touchstart",  function() {
        self.onMouseDown();
    }, false);
    this.dcanvas.addEventListener("touchend",  function() {
        self.onMouseUp();
    }, false);
    this.dcanvas.addEventListener("touchcancel",  function() {
        self.onMouseUp();
    }, false);
    this.run();

};

/**
 * mouse down callback.
 */
Sheep.prototype.onMouseDown = function() {
    this.add_sheep = true;
};

/**
 * mouse up callback.
 */
Sheep.prototype.onMouseUp = function() {
    this.add_sheep = false;
};

/**
 * stop animation.
 */
Sheep.prototype.stop = function() {
    this.runnable = false;
};

/**
 * run as infinite loop.
 */
Sheep.prototype.run = function() {
    var self = this;

    if(!this.canvas || !this.canvas.getContext) {
        return;
    }

    setTimeout(function() {
        self.calc();
        self.draw();

        self.dcanvas.getContext("2d").drawImage(self.canvas, 0, 0);

        if(self.runnable) {
            self.run();
        }
    }, 100);
};

/**
 * calculate the sheep position and so on.
 */
Sheep.prototype.calc = function() {

    if(this.add_sheep) {
        this.addSheep();
    }

    for(var i = 0; i < this.sheep_pos.length; i++) {
        if(this.sheep_pos[i][1] >= 0) {
            this.sheep_pos[i][0] -= 5;

            if((this.sheep_pos[i][3] - 20) < this.sheep_pos[i][0] && this.sheep_pos[i][0] <= this.sheep_pos[i][3]) {
                this.sheep_pos[i][1] -= 3;
            }

            else {
                if((this.sheep_pos[i][3] - 40) < this.sheep_pos[i][0] && this.sheep_pos[i][0] <= (this.sheep_pos[i][3] - 20)) {
                    this.sheep_pos[i][1] += 3;
                }
                if(this.sheep_pos[i][0] < (this.sheep_pos[i][3] - 10) && this.sheep_pos[i][2] === 0) {
                    this.sheep_number++;
                    this.sheep_pos[i][2] = 1;
                }
            }

            // remove a frameouted sheep
            if(this.sheep_pos[i][0] < -20) {
                if(i === 0) {
                    this.sheep_pos[0][0] =
                        (this.canvas.width - this.canvas.width % 10) + 10;
                    this.sheep_pos[0][2] = 0;
                    this.sheep_pos[0][3] = this.getJumpX(this.sheep_pos[0][1]);
                } else {
                    this.sheep_pos[i][0] = 0;
                    this.sheep_pos[i][1] = -1;
                    this.sheep_pos[i][2] = 0;
                    this.sheep_pos[i][3] = 0;
                }
            }

        }

    }

    this.action = 1 - this.action;
};

/** 
 * draw the sheep and the background images.
 */
Sheep.prototype.draw = function() {
    var ctx = this.canvas.getContext("2d"),
        l;

    ctx.beginPath();
    ctx.fillStyle = "rgb(120, 255, 120)";
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);

    ctx.beginPath();
    ctx.fillStyle = "rgb(150, 150, 255)";
    ctx.fillRect(0, 0, this.canvas.width,
        this.canvas.height - this.background.height + 10);

    ctx.drawImage(this.background, 40,
        this.canvas.height - this.background.height);

    for(l = 0; l < this.sheep_pos.length; l++) {
        if(this.sheep_pos[l][1] >= 0) {
            ctx.drawImage(this.sheep_image[this.action],
                this.sheep_pos[l][0], this.sheep_pos[l][1]);
        }
    }
    
    ctx.fillStyle = "rgb(0, 0, 0)";
    ctx.textBaseline = "top";
    ctx.fillText(this.sheep_number + " sheep", 2, 2);
};

/**
 * add new sheep.
 * @private
 */
Sheep.prototype.addSheep = function() {

    for(var i = 1; i < this.sheep_pos.length; i++) {

        if(this.sheep_pos[i][1] === -1) {
            this.sheep_pos[i][0] = (this.canvas.width - this.canvas.width % 10) + 10;
            this.sheep_pos[i][1] = (this.canvas.height - 70) + Math.floor(Math.random() * 60);
            this.sheep_pos[i][2] = 0;
            this.sheep_pos[i][3] = this.getJumpX(this.sheep_pos[i][1]);

            return;

        }

    }

};

/**
 * get jump position.
 * @private
 */
Sheep.prototype.getJumpX = function(y) {
    y -= (this.canvas.height - 40);
    return (70 - 3 * y / 4);
};

window.onload = function() {
    var sheep = new Sheep();
    sheep.start();
};

